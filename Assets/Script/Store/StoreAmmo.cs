using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StoreAmmo : ItemStore
{
    public Sprite m_AmmoArt;
    public int m_PriceItem;
    public int m_IndexAmmoType;
    public int m_SizeAmmo;
    public TextMeshProUGUI m_SizeTextAmmo;
    protected override void f_Display()
    {
        base.f_Display();
        m_ImageArt.sprite = m_AmmoArt;
        m_Price = m_PriceItem;
        m_PriceText.text = m_Price.ToString();
        m_ImageArt.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 2);
        m_BtnBuy.SetActive(true);
        m_SizeTextAmmo.text = "+ " + m_SizeAmmo.ToString();
    }
    public void f_BuyItemsAmmo()
    {
        if (PlayerStat.instance.m_CoinPlayer >= m_Price)
        {
            FindObjectOfType<SoundManager>().f_PlaySound("buy");
            PlayerStat.instance.m_CoinPlayer -= m_Price;
            switch (m_IndexAmmoType)
            {
                case 0:
                    PlayerStat.instance.m_Ammo9mm += m_SizeAmmo;
                    break;
                case 1:
                    PlayerStat.instance.m_Ammo55mm += m_SizeAmmo;
                    break;
                case 2:
                    PlayerStat.instance.m_Ammo12AGauge += m_SizeAmmo;
                    break;
            }
        }
    }
}
