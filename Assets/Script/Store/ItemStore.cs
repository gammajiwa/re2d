using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemStore : MonoBehaviour
{
    // Start is called before the first frame update
    public int m_ItemID;
    public Image m_ImageArt;
    public int m_Price;
    public GameObject m_BtnBuy;
    public TextMeshProUGUI m_PriceText;

    // public GameObject m_SpawnItem;


    // Update is called once per frame
    void Update()
    {
        f_Display();
        f_AktivedBTN();
    }

    protected virtual void f_Display()
    {
        foreach (Weapons m_ItemData in PlayerStat.instance.m_ListWeapons)
        {
            if (m_ItemData.m_ID == m_ItemID)
            {
                m_ImageArt.sprite = m_ItemData.m_Image;
                m_Price = m_ItemData.m_Price;
                m_PriceText.text = m_Price.ToString();
                m_BtnBuy.SetActive(true);
                for (int i = 0; i < PlayerStat.instance.m_OwnedWeapon.Count; i++)
                {

                    if (PlayerStat.instance.m_OwnedWeapon[i] == m_ItemID)
                    {
                        m_PriceText.text = "Owned";
                        m_BtnBuy.SetActive(false);
                    }
                }
            }
        }
    }


    public virtual void f_BuyItems()
    {

        if (PlayerStat.instance.m_CoinPlayer >= m_Price)
        {
            PlayerStat.instance.m_CoinPlayer -= m_Price;
            FindObjectOfType<SoundManager>().f_PlaySound("buy");
            f_SpawnGuns();

        }
    }

    void f_SpawnGuns()
    {
        PlayerStat.instance.m_OwnedWeapon.Add(m_ItemID);
        // UIManager.instance.f_SpawnGuns();
        UIManager.instance.f_ActivatedTabGun();
    }

    void f_AktivedBTN()
    {
        if (PlayerStat.instance.m_CoinPlayer >= m_Price)
            m_BtnBuy.GetComponent<Button>().interactable = true;
        else
            m_BtnBuy.GetComponent<Button>().interactable = false;

    }


}
