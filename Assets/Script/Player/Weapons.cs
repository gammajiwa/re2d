using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "new Weapons", menuName = "Weapon")]
public class Weapons : ScriptableObject
{
    public Sprite m_Image;
    public GameObject m_MuzzleFlash;
    public AudioClip m_Sfx;
    public Vector2 m_MuzzlePos;
    public int m_ID;
    public int m_Level;
    public int m_Exp;
    public int m_CurrentAmmo;
    public string m_IdName;
    public float m_FireRateDefault;
    public float m_ReloadTimeDefault;
    public float m_RecoilDefault;
    public float m_BulletSpeedDefault;
    public float m_BulletDropDefault;
    public int m_MinDamageDefault;
    public int m_MaxDamageDefault;
    public int m_MagazineDefault;
    public int m_TargetLevelUpDefault;
    public int m_BulletSpray;
    public int m_Price;


    [HideInInspector]
    public float m_FireRate;
    [HideInInspector]
    public float m_ReloadTime;
    [HideInInspector]
    public float m_Recoil;
    [HideInInspector]
    public float m_BulletSpeed;
    [HideInInspector]
    public float m_BulletDrop;
    [HideInInspector]
    public int m_MinDamage;
    [HideInInspector]
    public int m_MaxDamage;

    [HideInInspector]
    public int m_Magazine;
    [HideInInspector]
    int m_TargetLevelUp;


    public enum e_AmmoType
    {
        Ammo_9mm,
        Ammo_55mm,
        Ammo_12Gauge
    }
    public enum e_GunsType
    {
        Pistol,
        AR,
        ShotGun
    }
    public e_AmmoType m_AmmoType;
    public e_GunsType m_GunsType;


    public void f_WeaponDefault()
    {
        m_Level = 1;
        m_Exp = 0;
        m_CurrentAmmo = m_MagazineDefault;
        m_FireRate = m_FireRateDefault;
        m_ReloadTime = m_ReloadTimeDefault;
        m_Recoil = m_RecoilDefault;
        m_BulletSpeed = m_BulletSpeedDefault;
        m_BulletDrop = m_BulletDropDefault;
        m_MinDamage = m_MinDamageDefault;
        m_MaxDamage = m_MaxDamageDefault;
        m_Magazine = m_MagazineDefault;
        m_TargetLevelUp = m_TargetLevelUpDefault;
    }



}
