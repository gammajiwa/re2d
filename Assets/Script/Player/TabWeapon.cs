using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TabWeapon : MonoBehaviour
{
    // Start is called before the first frame update
    public int m_Id;
    public Image m_Image;
    // public TextMeshProUGUI m_StatText;
    string m_TextStat;

    public Sprite[] m_LevelBox;
    public Image m_CurrLevelBox;


    void Update()
    {
        m_Image.sprite = PlayerStat.instance.m_ListWeapons[m_Id].m_Image;
        m_CurrLevelBox.sprite = m_LevelBox[PlayerStat.instance.m_ListWeapons[m_Id].m_Level - 1];

    }


    public void f_ChangeWeapon()
    {
        Button m_btn = gameObject.GetComponentInChildren<Button>();
        // m_btn.spriteState.pressedSprite = "btn_red";
        Guns.instance.f_CurrentWeapons(m_Id);
    }

    public void f_DestroyWeapon()
    {
        if (Guns.instance.m_IndexWeapons == m_Id)
        {
            Guns.instance.f_CurrentWeapons(PlayerStat.instance.m_OwnedWeapon[0]);
        }
        PlayerStat.instance.m_OwnedWeapon.Remove(m_Id);
        Debug.Log(m_Id);
        // Destroy(gameObject);
        gameObject.SetActive(false);
    }

    public void f_StatInfo()
    {
        UIManager.instance.m_StatGuns.SetActive(true);
        GameObject m_TextMeshStat = GameObject.FindGameObjectWithTag("stattext");
        Weapons m_Item = PlayerStat.instance.m_ListWeapons[m_Id];
        m_TextStat = "Name : " + m_Item.m_IdName;
        m_TextStat += "<br>Lvl : " + m_Item.m_Level;
        m_TextStat += "<br>Type : " + m_Item.m_AmmoType;
        m_TextStat += "<br>Dmg : " + m_Item.m_MinDamage + "-" + m_Item.m_MaxDamage;
        m_TextStat += "<br>Fire Rate : " + m_Item.m_FireRate;
        m_TextStat += "<br>Reload : " + m_Item.m_ReloadTime;
        m_TextStat += "<br>Mag : " + m_Item.m_Magazine;
        m_TextStat += "<br>Recoil : " + m_Item.m_Recoil;
        m_TextStat += "<br>Bullet Speed : " + m_Item.m_BulletSpeed;
        m_TextStat += "<br>Bullet Drop : " + m_Item.m_BulletDrop;
        m_TextMeshStat.GetComponent<TextMeshProUGUI>().text = m_TextStat;

    }


}
