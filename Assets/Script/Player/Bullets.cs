using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullets : MonoBehaviour
{
    public int m_OnHit;
    public float m_Timer;


    private void Update()
    {
        m_Timer -= 1 * Time.deltaTime;
        if (m_Timer <= 0)
            gameObject.SetActive(false);
    }
    public void f_Init(int p_Onhit, float p_Timer)
    {
        m_OnHit = p_Onhit;
        m_Timer = p_Timer;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (m_OnHit == 0)
        {
            if (other.gameObject.tag == "enemy")
            {
                other.transform.GetComponentInParent<Enemys>().f_Damage(this.gameObject.transform, Guns.instance.m_MinDamage, Color.white);
                gameObject.SetActive(false);
                m_OnHit++;
            }
            if (other.gameObject.tag == "head")
            {
                {
                    other.transform.GetComponentInParent<Enemys>().f_Damage(this.gameObject.transform, Guns.instance.m_MaxDamage, Color.red);
                    gameObject.SetActive(false);
                    m_OnHit++;
                }
            }
        }
    }
}



