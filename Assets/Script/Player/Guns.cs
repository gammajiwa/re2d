using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guns : MonoBehaviour
{
    // Start is called before the first frame update
    public static Guns instance;
    // public List<Weapons> m_ListWeapons = new List<Weapons>();
    // public List<int> m_OwnedWeapon = new List<int>();

    public Weapons m_MyWeapon;


    public float m_DestroyBullet;
    public float m_BulletSpeed;

    public int m_Magazine;
    public int m_MinDamage;
    public int m_MaxDamage;
    public int m_IndexWeapons;
    public float m_ReloadTime;

    public GameObject m_Bullets;
    public Transform m_SpawnBullets;
    public Transform m_Gun;
    public SpriteRenderer m_ArtGuns;


    // public int m_Ammo9mm;
    // public int m_Ammo55mm;
    // public int m_Ammo12AGauge;




    bool m_Brush;
    bool m_Reload;

    float m_NextFire = 0;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            // DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);

    }

    private void Start()
    {
        foreach (var item in PlayerStat.instance.m_ListWeapons)
        {
            item.f_WeaponDefault();
        }
        f_CurrentWeapons(0);
        GameManager.instance.f_LoadGame();

    }

    // Update is called once per frame
    void Update()
    {
        f_GunRotation(m_Gun, 0);

        f_GunStatus();

        if (!m_Reload)
        {
            if (GameManager.instance.m_MyState == GameManager.e_GameState.wave)
            {
                if (Input.GetMouseButton(1) && Time.time > m_NextFire && m_MyWeapon.m_CurrentAmmo > 0)
                {
                    m_NextFire = Time.time + m_MyWeapon.m_FireRate;
                    shoot();
                    m_Brush = true;
                    FindObjectOfType<SoundManager>().f_PlaySound("SfxGun");
                }
                else if (Input.GetMouseButtonDown(1) && m_MyWeapon.m_CurrentAmmo == 0)
                {
                    FindObjectOfType<SoundManager>().f_PlaySound("noammo");
                    m_NextFire = Time.time;
                }
                if (Input.GetMouseButtonUp(1))
                {
                    if ((int)m_MyWeapon.m_GunsType != 2)
                        m_NextFire = Time.time - 1;
                }
            }
        }

        f_Reloading();
        if (Input.GetMouseButtonUp(0))
        {
            m_Brush = false;
        }
    }



    void shoot()
    {
        m_MyWeapon.m_CurrentAmmo--;
        for (int i = 0; i < m_MyWeapon.m_BulletSpray; i++)
        {
            f_GunRotation(m_SpawnBullets, m_MyWeapon.m_Recoil);
            // GameObject m_Bullet = Instantiate(m_Bullets, m_SpawnBullets.position, m_SpawnBullets.rotation);
            GameObject m_Bullet = ObjectPool.instance.f_SpanwFromPool("bullet", m_SpawnBullets.position, m_SpawnBullets.rotation);
            m_Bullet.GetComponent<Rigidbody2D>().velocity = m_Bullet.transform.right * m_BulletSpeed;
            m_Bullet.GetComponent<Rigidbody2D>().gravityScale = m_MyWeapon.m_BulletDrop;
            m_Bullet.GetComponent<Bullets>().f_Init(0, m_DestroyBullet);
            // Destroy(m_Bullet, m_DestroyBullet);

        }
        GameObject m_Flash = ObjectPool.instance.f_SpanwFromPool("flash", m_SpawnBullets.position, m_SpawnBullets.rotation);
        FindObjectOfType<SoundManager>().f_PlaySound("SfxGun");
        m_Flash.GetComponent<TimerObj>().f_Init(0.2f);

    }





    void f_GunRotation(Transform p_Dir, float p_Rand)
    {
        Vector2 m_GunPos = p_Dir.position;
        Vector2 m_TouchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 m_Dir = m_TouchPos - m_GunPos;
        if (m_Brush || (int)m_MyWeapon.m_GunsType == 2)
        {
            m_Dir = new Vector2(m_Dir.x, m_Dir.y + Random.Range(-p_Rand, p_Rand));
        }
        p_Dir.transform.right = m_Dir;
        m_SpawnBullets.localPosition = m_MyWeapon.m_MuzzlePos;
    }






    public void f_CurrentWeapons(int p_Index)
    {
        m_MyWeapon = PlayerStat.instance.m_ListWeapons[p_Index];
        m_IndexWeapons = p_Index;
        foreach (Sound item in SoundManager.instance.m_Sounds)
        {
            if (item.m_name == "SfxGun")
            {
                item.m_Clip = m_MyWeapon.m_Sfx;
                item.m_Sourc.clip = item.m_Clip;
            }
        }
        FindObjectOfType<SoundManager>().f_PlaySound("ready");
    }




    void f_GunStatus()
    {
        m_MinDamage = m_MyWeapon.m_MinDamage;
        m_MaxDamage = m_MyWeapon.m_MaxDamage;
        // m_MaxAmmo = m_MyWeapon.m_MaxAmmo;
        m_BulletSpeed = m_MyWeapon.m_BulletSpeed;
        m_Magazine = m_MyWeapon.m_Magazine;
        m_ArtGuns.sprite = m_MyWeapon.m_Image;
    }



    public void f_Reload()
    {
        if (f_CheckAmmoInStorage())
        {
            m_Reload = true;
            m_ReloadTime = m_MyWeapon.m_ReloadTime;
            FindObjectOfType<SoundManager>().f_PlaySound("reload");
        }
    }

    bool f_CheckAmmoInStorage()
    {
        if ((int)m_MyWeapon.m_AmmoType == 0)
        {
            if (PlayerStat.instance.m_Ammo9mm > 0)
            {
                return true;
            }
        }
        if ((int)m_MyWeapon.m_AmmoType == 1)
        {
            if (PlayerStat.instance.m_Ammo55mm > 0)
            {
                return true;
            }
        }
        if ((int)m_MyWeapon.m_AmmoType == 2)
        {
            if (PlayerStat.instance.m_Ammo12AGauge > 0)
            {
                return true;
            }
        }
        return false;
    }




    // void f_OwnedWeapon()
    // {
    //     foreach (Weapons m_Wepon in m_ListWeapons)
    //     {
    //         m_OwnedWeapon.Add(m_Wepon.m_ID);
    //     }
    // }

    void f_Reloading()
    {
        if (m_Reload)
        {
            m_ReloadTime = m_ReloadTime - Time.deltaTime;
            int m_AmmoAmount = 0;
            if (m_ReloadTime <= 0)
            {
                m_Reload = false;
                PlayerStat m_Player = PlayerStat.instance;
                switch ((int)m_MyWeapon.m_AmmoType)
                {
                    case 0:
                        if (m_Player.m_Ammo9mm + m_MyWeapon.m_CurrentAmmo > m_MyWeapon.m_Magazine)
                        {
                            m_Player.m_Ammo9mm += m_MyWeapon.m_CurrentAmmo;
                            m_Player.m_Ammo9mm -= m_MyWeapon.m_Magazine;
                            m_AmmoAmount = m_MyWeapon.m_Magazine;
                        }
                        else
                        {
                            m_AmmoAmount = m_Player.m_Ammo9mm + m_MyWeapon.m_CurrentAmmo;
                            m_Player.m_Ammo9mm = 0;
                        }
                        break;
                    case 1:
                        if (m_Player.m_Ammo55mm + m_MyWeapon.m_CurrentAmmo > m_MyWeapon.m_Magazine)
                        {
                            m_Player.m_Ammo55mm += m_MyWeapon.m_CurrentAmmo;
                            m_Player.m_Ammo55mm -= m_MyWeapon.m_Magazine;
                            m_AmmoAmount = m_MyWeapon.m_Magazine;
                        }
                        else
                        {
                            m_AmmoAmount = m_Player.m_Ammo55mm + m_MyWeapon.m_CurrentAmmo;
                            m_Player.m_Ammo55mm = 0;
                        }
                        break;
                    case 2:
                        if (m_Player.m_Ammo12AGauge + m_MyWeapon.m_CurrentAmmo > m_MyWeapon.m_Magazine)
                        {
                            m_Player.m_Ammo12AGauge += m_MyWeapon.m_CurrentAmmo;
                            m_Player.m_Ammo12AGauge -= m_MyWeapon.m_Magazine;
                            m_AmmoAmount = m_MyWeapon.m_Magazine;
                        }
                        else
                        {
                            m_AmmoAmount = m_Player.m_Ammo12AGauge + m_MyWeapon.m_CurrentAmmo;
                            m_Player.m_Ammo12AGauge = 0;
                        }
                        break;
                }
                m_MyWeapon.m_CurrentAmmo = m_AmmoAmount;
                FindObjectOfType<SoundManager>().f_StopSound("reload");
                FindObjectOfType<SoundManager>().f_PlaySound("ready");
            }
        }
    }



}

