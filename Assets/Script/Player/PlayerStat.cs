using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStat : MonoBehaviour
{
    public static PlayerStat instance;
    public List<Weapons> m_ListWeapons = new List<Weapons>();
    public List<int> m_OwnedWeapon = new List<int>();
    public int m_Ammo9mm;
    public int m_Ammo55mm;
    public int m_Ammo12AGauge;
    public int m_CoinPlayer;


    public int m_IndexSkinHair;
    public int m_IndexSkinChest;
    public int m_IndexSkinHelmet;
    public int m_IndexSkinMask;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);

    }

}
