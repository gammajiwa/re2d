using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    // Start is called before the first frame update
    Transform m_Player;
    public SpriteRenderer m_MainImage;
    public Sprite[] m_AmmoImage;
    public Sprite m_CoinImage;
    public Sprite m_HPImage;
    public int m_DropAmmo;
    public int m_DropCoin;
    public int m_DropHP;
    public TrailRenderer m_Trail;
    e_ItemType m_ItemType;
    int m_IndexAmmo;
    int m_Percentage;

    enum e_ItemType
    {
        Ammo,
        Coin,
        Health
    }

    void Start()
    {
        m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        f_Percentage();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, m_Player.position) > 0.1f)
        {
            transform.position = Vector2.MoveTowards(transform.position, m_Player.position, 25 * Time.deltaTime);
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (m_ItemType == e_ItemType.Ammo)
            {
                switch (m_IndexAmmo)
                {
                    case 1:
                        PlayerStat.instance.m_Ammo9mm += m_DropAmmo;

                        break;
                    case 2:
                        PlayerStat.instance.m_Ammo55mm += m_DropAmmo;

                        break;
                    case 3:
                        PlayerStat.instance.m_Ammo12AGauge += m_DropAmmo;

                        break;
                }
            }
            if (m_ItemType == e_ItemType.Coin)
            {
                PlayerStat.instance.m_CoinPlayer += m_DropCoin;
                m_MainImage.sprite = m_CoinImage;
            }
            if (m_ItemType == e_ItemType.Health)
            {
                GameManager.instance.m_HPPlayer += m_DropHP;

            }
            Destroy(gameObject);
        }
    }

    void f_Percentage()
    {
        gameObject.transform.localScale = new Vector3(1, 1, 1);
        m_Percentage = Random.Range(1, 101);
        if (m_Percentage < 65)
        {
            m_ItemType = e_ItemType.Coin;
            m_MainImage.sprite = m_CoinImage;
            m_Trail.startColor = Color.yellow;
        }
        else if ((m_Percentage > 66 && m_Percentage < 95))
        {
            m_ItemType = e_ItemType.Ammo;
            m_IndexAmmo = Random.Range(1, 4);
            // Debug.Log(m_IndexAmmo);
            m_MainImage.sprite = m_AmmoImage[m_IndexAmmo - 1];
            m_Trail.startColor = Color.red;
            gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 1);
        }
        else
        {
            m_ItemType = e_ItemType.Health;
            m_MainImage.sprite = m_HPImage;
            m_Trail.startColor = Color.white;
        }
        // Debug.Log(m_Percentage);
    }



}
