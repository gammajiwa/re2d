using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemys : MonoBehaviour
{
    public GameObject m_BloodVFX;
    public GameObject m_PopUpScore;
    public Animator m_Anime;
    public GameObject m_DropItem;


    public int m_HpEnmenyDefault;
    public float m_SpeedDefault;
    public float m_AttckRateDefault;
    public float m_DamageEnemyDefault;
    public float m_AttackRangDefault;

    int m_HpEnmeny;
    float m_Speed;
    float m_AttckRate;
    float m_DamageEnemy;
    float m_AttackRang;


    Transform m_Player;
    public bool m_life = true;
    int m_DropItemPossibility;
    float m_NextAttck;

    public void f_Init()
    {
        m_HpEnmeny = m_HpEnmenyDefault;
        m_Speed = m_SpeedDefault;
        m_AttckRate = m_AttckRateDefault;
        m_DamageEnemy = m_DamageEnemyDefault;
        m_AttackRang = m_AttackRangDefault;
        m_life = true;
        StopCoroutine(f_Destroy());
        f_CollisionEnemy(true);
        f_DropItemPossibility();
        m_Anime.Play("Walk");

    }

    private void OnEnable()
    {
        f_Init();
    }
    void Start()
    {
        m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.m_MyState == GameManager.e_GameState.wave)
        {
            if (m_life)
            {
                f_Death();
                f_Movement();
                f_Attack();
            }
        }
        if (GameManager.instance.m_MyState == GameManager.e_GameState.Run)
            gameObject.transform.Translate(Vector3.left * GameManager.instance.m_SpeedMovement * Time.deltaTime);

    }



    void f_Death()
    {
        if (m_HpEnmeny <= 0)
        {
            m_life = false;
            UIManager.instance.m_EnemyKill++;
            GameManager.instance.m_EnemysCount--;
            m_Anime.Play("Dead");
            f_CollisionEnemy(false);
            StartCoroutine(f_Destroy());

        }
    }


    public void f_Damage(Transform p_Bullet, int p_Damage, Color pColor)
    {
        GameObject m_Vfx = ObjectPool.instance.f_SpanwFromPool("blood", p_Bullet.position, Quaternion.identity);
        // GameObject m_Score = Instantiate(m_PopUpScore, p_Bullet.position + new Vector3(Random.Range(-0.5f, 1f), 0f, 0f), Quaternion.identity);
        GameObject m_Score = ObjectPool.instance.f_SpanwFromPool("score", p_Bullet.position + new Vector3(Random.Range(-0.5f, 1f), 0f, 0f), Quaternion.identity);
        m_Score.GetComponentInChildren<TextMesh>().text = "-" + p_Damage.ToString();
        m_Score.GetComponentInChildren<TextMesh>().color = pColor;
        m_Score.GetComponentInChildren<MeshRenderer>().sortingOrder = 300;
        m_HpEnmeny -= p_Damage;
        if (pColor == Color.red)
        {
            m_Vfx.GetComponentInChildren<Animator>().Play("blood2");
            FindObjectOfType<SoundManager>().f_PlaySound("hit2");
            UIManager.instance.m_EnemyHeadShoot++;
        }
        else if (pColor == Color.white)
        {
            m_Vfx.GetComponentInChildren<Animator>().Play("blood");
            FindObjectOfType<SoundManager>().f_PlaySound("hit1");
        }

        // Destroy(m_Vfx, 2);
        // // Destroy(m_Score, 1);
        m_Vfx.GetComponent<TimerObj>().f_Init(2f);
        m_Score.GetComponent<TimerObj>().f_Init(1f);
    }


    void f_Movement()
    {
        if (Vector3.Distance(transform.position, m_Player.position) > m_AttackRang - 1)
        {
            transform.position = Vector2.MoveTowards(transform.position, m_Player.position, m_Speed * Time.deltaTime);

        }
        // f_Attack();
    }

    IEnumerator f_Destroy()
    {
        yield return new WaitForSeconds(2f);
        if (m_DropItemPossibility <= 50)
        {
            Instantiate(m_DropItem, transform.position, Quaternion.identity);
        }
        gameObject.SetActive(false);

    }

    int f_DropItemPossibility()
    {
        m_DropItemPossibility = Random.Range(1, 101);
        return m_DropItemPossibility;
    }


    void f_Attack()
    {
        if (Vector3.Distance(transform.position, m_Player.position) <= m_AttackRang)
        {
            if (Time.time > m_NextAttck)
            {
                m_NextAttck = Time.time + m_AttckRate;
                m_Anime.Play("Attack");
                f_DamageToPlayer();
            }
        }
    }

    void f_DamageToPlayer()
    {
        GameManager.instance.m_HPPlayer -= m_DamageEnemy;
        // GameObject m_Score = Instantiate(m_PopUpScore, m_Player.position + new Vector3(Random.Range(-0.5f, 1f), 2f, 0f), Quaternion.identity);
        GameObject m_Score = ObjectPool.instance.f_SpanwFromPool("score", m_Player.position + new Vector3(Random.Range(-0.5f, 1f), 2f, 0f), Quaternion.identity);
        m_Score.GetComponentInChildren<TextMesh>().text = "-" + m_DamageEnemy.ToString();
        m_Score.GetComponentInChildren<TextMesh>().color = Color.black;
        m_Score.GetComponentInChildren<MeshRenderer>().sortingOrder = 300;
        m_Score.GetComponent<TimerObj>().f_Init(1f);
    }

    public void f_CollisionEnemy(bool p_Result)
    {
        BoxCollider2D[] m_CollEnemy = gameObject.GetComponentsInChildren<BoxCollider2D>();
        foreach (var item in m_CollEnemy)
        {
            item.enabled = p_Result;
        }
    }



}
