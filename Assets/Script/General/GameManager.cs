using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameManager instance;
    public Animator m_Anim;
    public Transform[] m_PlatForms;
    public GameObject[] m_Enemys;
    public GameObject m_Finish;

    public Transform m_SpawnPoint;

    public float m_SpeedMovement;
    public int m_EnemySize;
    public int m_EnemysCount;
    public float m_NextSpawn;
    public float m_NextWave;
    public float m_SlowFactor;
    public float m_SlowLength;
    public float m_LengthTrack;
    public int m_FirstLogin;
    public int m_GiveStar;



    bool m_Puase;
    bool m_BGM;
    bool m_Sfx;



    public float m_HPPlayer;



    public enum e_GameState
    {
        wave,
        Run,
        GameOver,
        Menu,
        Pause,
        Finish,
        EndGame
    }
    public e_GameState m_MyState;

    // Player stat

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            // DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
        m_MyState = e_GameState.Menu;
    }

    private void Start()
    {
        SaveManager.instance.f_LoadByJSON();
    }


    void Update()
    {
        if (m_MyState != e_GameState.Menu)
        {
            if (m_MyState == e_GameState.Run || m_MyState == e_GameState.EndGame)
            {
                f_PlatFormMove();
                m_NextWave -= 1 * Time.deltaTime;
            }

            if (m_MyState == e_GameState.wave)
            {
                m_Anim.Play("Ready" + Guns.instance.m_MyWeapon.m_GunsType);
                m_Anim.Play("idle");
                f_SpawnEnemy();
                f_TimeControl();
            }
            f_NextWave();
            f_LengthTrack();
            f_EndGame();


            if (m_HPPlayer >= 100)
                m_HPPlayer = 100;

            if (m_HPPlayer <= 0)
                f_GameOver();
        }

        // f_GiveStar();
    }




    void f_PlatFormMove()
    {
        m_Anim.Play("Run");
        for (int i = 0; i < m_PlatForms.Length; i++)
        {
            m_PlatForms[i].transform.Translate(Vector3.left * m_SpeedMovement * Time.deltaTime);
            if (m_PlatForms[i].transform.localPosition.x < -80.3f)
            {
                m_PlatForms[i].transform.localPosition = new Vector2(80, 0);
            }
        }
    }


    void f_SpawnEnemy()
    {
        if (Time.time > m_NextSpawn && m_EnemySize > 0)
        {
            m_NextSpawn = Time.time + Random.Range(0.5f, 1.5f);
            // Instantiate(m_Enemys[0], m_SpawnPoint.position + new Vector3(0, Random.Range(-1, 1), 0), Quaternion.identity);
            GameObject m_Enemy = ObjectPool.instance.f_SpanwFromPool("enemy", m_SpawnPoint.position + new Vector3(0, Random.Range(-1, 1), 0), Quaternion.identity);
            // m_Enemy.GetComponentInChildren<Enemys>().f_Init();
            m_EnemysCount++;
            m_EnemySize--;
        }
    }



    void f_TimeControl()
    {
        Time.timeScale += (1f / m_SlowLength) * Time.unscaledDeltaTime;
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
    }

    void f_NextWave()
    {

        if (m_EnemysCount <= 0 && m_EnemySize <= 0 && m_MyState != e_GameState.EndGame && m_MyState != e_GameState.Finish)
            m_MyState = e_GameState.Run;
        if (m_NextWave <= 0 && m_LengthTrack > 5)
        {
            m_MyState = e_GameState.wave;
            m_NextWave = Random.Range(10, 20f);
            m_EnemySize = Random.Range(5, 15);
        }
    }


    public void f_SlowMotion()
    {
        Time.timeScale = m_SlowFactor;
        Time.fixedDeltaTime = Time.timeScale * 0.2f;
    }


    public void f_Pause()
    {
        if (m_Puase)
        {
            m_Puase = false;
            Time.timeScale = 1;
            if (m_EnemysCount > 0)
                m_MyState = e_GameState.wave;
            else
                m_MyState = e_GameState.Run;
        }
        else
        {
            m_Puase = true;
            m_MyState = e_GameState.Pause;
            Time.timeScale = 0;
        }
    }

    void f_GameOver()
    {
        m_MyState = e_GameState.GameOver;

    }

    public void f_ResetBtn()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        f_LoadGame();
        FindObjectOfType<SoundManager>().f_PlaySound("click");

    }
    public void f_BackHome()
    {
        SceneManager.LoadScene("Home");
        FindObjectOfType<SoundManager>().f_PlaySound("click");
    }
    public void f_WinSateg()
    {
        m_FirstLogin = 1;

        if (LevelStageManager.instance.m_CurrLevel == LevelStageManager.instance.m_UnlockStage)
        {
            LevelStageManager.instance.m_UnlockStage++;
        }
        SaveManager.instance.f_SaveByJSON();
        SceneManager.LoadScene("Home");
        FindObjectOfType<SoundManager>().f_PlaySound("click");
    }

    public void f_LoadGame()
    {

        if (m_FirstLogin != 0)
        {
            SaveManager.instance.f_LoadByJSON();
        }
        else
        {
            m_MyState = e_GameState.Run;
            m_HPPlayer = 100;
            PlayerStat.instance.m_CoinPlayer = 10000;
            m_EnemysCount = 0;
            m_EnemySize = 0;
            m_NextWave = 0;
            Time.timeScale = 1;
            m_LengthTrack = Random.Range(30, 50);
            PlayerStat.instance.m_Ammo9mm = 120;
        }
        // 
    }

    void f_LengthTrack()
    {
        if (m_MyState == e_GameState.Run)
        {
            m_LengthTrack -= 1 * Time.deltaTime;
        }

    }

    void f_EndGame()
    {

        if (m_MyState != e_GameState.Finish && m_MyState != e_GameState.EndGame)
        {
            if (m_LengthTrack <= 0)
            {
                m_MyState = e_GameState.EndGame;
                GameObject Finish = Instantiate(m_Finish, m_SpawnPoint.position, transform.rotation);
                Finish.GetComponent<Rigidbody2D>().velocity = Finish.transform.right * -m_SpeedMovement;
            }

        }
    }


    public void f_ClsoeApp()
    {
        Application.Quit();
        FindObjectOfType<SoundManager>().f_PlaySound("click");
    }


    public void f_SetVolume(float p_Volume)
    {
        AudioListener.volume = p_Volume;
    }


    public void f_GiveStar()
    {
        if (m_HPPlayer == 100)
        {
            m_GiveStar = 3;
        }
        else if (m_HPPlayer >= 50 && m_HPPlayer < 100)
        {
            m_GiveStar = 2;
        }
        else
        {
            m_GiveStar = 1;
        }

        LevelStageManager.instance.m_AllStart[LevelStageManager.instance.m_CurrLevel - 1] = m_GiveStar;

    }



    public void f_StarDisplay(Image[] p_ImageStars, Sprite[] p_SpriteImage, int p_Size)
    {

        foreach (var item in p_ImageStars)
        {
            item.sprite = p_SpriteImage[1];
        }
        for (int i = 0; i < p_Size; i++)
        {
            p_ImageStars[i].sprite = p_SpriteImage[0];
        }
    }
}

