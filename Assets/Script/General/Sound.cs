using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
[System.Serializable]
public class Sound
{
    public AudioClip m_Clip;
    public string m_name;
    public float m_Volume;
    public float m_Pitch;
    public bool m_Loop;

    [HideInInspector]
    public AudioSource m_Sourc;
}
