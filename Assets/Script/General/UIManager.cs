using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public TextMeshProUGUI m_AmmoText;
    public TextMeshProUGUI m_MaxAmmoText;
    public TextMeshProUGUI m_CurrLevel;
    public Image m_TopGunsUI;
    public GameObject m_StatGuns;
    public TextMeshProUGUI m_9mm;
    public TextMeshProUGUI m_55mm;
    public TextMeshProUGUI m_12gauge;
    public TextMeshProUGUI m_CoinPlayer;
    public Image m_PlayerHP;
    public GameObject m_Seting;
    public GameObject m_Store;
    public GameObject m_GameOver;

    public GameObject[] m_ShopCategory;
    public GameObject[] m_GunsTab;
    public ScrollRect m_ScrolStore;
    public GameObject m_SpawnItem;
    public GameObject m_WinTab;

    public Sprite[] m_StarSprite;
    public Image[] m_StarImage;
    public int m_EnemyKill;
    public int m_EnemyHeadShoot;
    public TextMeshProUGUI[] m_EnemyKillText;
    public TextMeshProUGUI[] m_EnemyHeadShootText;




    // Update is called once per frame
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            // DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
        f_ActivatedStore(0);
    }

    private void Start()
    {
        // f_SpawnGuns();
        f_ActivatedTabGun();
    }


    void Update()
    {
        m_AmmoText.text = Guns.instance.m_MyWeapon.m_CurrentAmmo.ToString();
        m_TopGunsUI.sprite = Guns.instance.m_MyWeapon.m_Image;
        m_CoinPlayer.text = PlayerStat.instance.m_CoinPlayer.ToString();
        m_PlayerHP.fillAmount = GameManager.instance.m_HPPlayer / 100;
        m_9mm.text = PlayerStat.instance.m_Ammo9mm.ToString();
        m_55mm.text = PlayerStat.instance.m_Ammo55mm.ToString();
        m_12gauge.text = PlayerStat.instance.m_Ammo12AGauge.ToString();
        m_CurrLevel.text = LevelStageManager.instance.m_CurrLevel.ToString();
        f_AllAmmo();
        f_WinTab();
        f_UiScore();




        if (GameManager.instance.m_MyState == GameManager.e_GameState.GameOver)
            m_GameOver.SetActive(true);
        else m_GameOver.SetActive(false);
    }


    // public void f_ChangeWeapon(int p_Index)
    // {
    //     Debug.Log("jalan");
    //     GameObject m_Btn = Instantiate(m_BtnPrefab);
    //     m_Btn.transform.parent = m_TabParent.transform;
    //     m_Btn.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
    //     m_Btn.GetComponentInChildren<Button>().onClick.AddListener(() => f_ChangeWeapon(1));
    // }


    public void f_CloseGunsStat()
    {
        m_StatGuns.SetActive(false);
    }

    void f_AllAmmo()
    {

        switch ((int)Guns.instance.m_MyWeapon.m_AmmoType)
        {
            case 0:
                m_MaxAmmoText.text = "/" + PlayerStat.instance.m_Ammo9mm.ToString();
                break;
            case 1:
                m_MaxAmmoText.text = "/" + PlayerStat.instance.m_Ammo55mm.ToString();
                break;
            case 2:
                m_MaxAmmoText.text = "/" + PlayerStat.instance.m_Ammo12AGauge.ToString();
                break;
        }
    }

    public void f_Seting()
    {
        m_Seting.SetActive(!m_Seting.activeSelf);
        GameManager.instance.f_Pause();

    }
    public void f_Store()
    {
        m_Store.SetActive(!m_Store.activeSelf);
    }


    public void f_ActivatedStore(int p_Index)
    {
        foreach (var item in m_ShopCategory)
        {
            item.SetActive(false);
        }
        m_ShopCategory[p_Index].SetActive(true);
        m_ScrolStore.content = m_ShopCategory[p_Index].GetComponent<RectTransform>();
    }



    public void f_SpawnGuns()
    {
        GameObject[] m_TabWeapons = GameObject.FindGameObjectsWithTag("Tabweapons");
        foreach (var item in m_TabWeapons)
        {
            Destroy(item);
        }

        foreach (var m_ItemID in PlayerStat.instance.m_OwnedWeapon)
        {
            GameObject m_Item = Instantiate(m_SpawnItem);
            m_Item.GetComponent<TabWeapon>().m_Id = m_ItemID;
            m_Item.transform.parent = GameObject.FindGameObjectWithTag("spawngun").transform;
            m_Item.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
        }

    }


    public void f_ActivatedTabGun()
    {
        foreach (var item in m_GunsTab)
        {
            item.SetActive(false);
        }
        for (int i = 0; i < PlayerStat.instance.m_OwnedWeapon.Count; i++)
        {

            m_GunsTab[i].SetActive(true);
            m_GunsTab[i].GetComponent<TabWeapon>().m_Id = PlayerStat.instance.m_OwnedWeapon[i];
        }
    }


    void f_WinTab()
    {
        if (GameManager.instance.m_MyState != GameManager.e_GameState.Finish)
            m_WinTab.SetActive(false);
        else
        {
            m_WinTab.SetActive(true);
            GameManager.instance.f_GiveStar();
            GameManager.instance.f_StarDisplay(m_StarImage, m_StarSprite, GameManager.instance.m_GiveStar);
        }

    }

    void f_UiScore()
    {
        for (int i = 0; i < 2; i++)
        {
            m_EnemyKillText[i].text = m_EnemyKill.ToString();
            m_EnemyHeadShootText[i].text = m_EnemyHeadShoot.ToString();

        }
    }




}


