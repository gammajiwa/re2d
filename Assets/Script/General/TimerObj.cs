using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerObj : MonoBehaviour
{
    public float m_Timer;

    public void f_Init(float p_Time)
    {
        m_Timer = p_Time;
    }
    private void Update()
    {
        m_Timer -= 1 * Time.deltaTime;
        if (m_Timer <= 0)
            gameObject.SetActive(false);
    }
}
