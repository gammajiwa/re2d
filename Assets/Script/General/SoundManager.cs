using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    public Sound[] m_Sounds;



    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);


        foreach (Sound m_Sound in m_Sounds)
        {
            m_Sound.m_Sourc = gameObject.AddComponent<AudioSource>();
            m_Sound.m_Sourc.name = m_Sound.m_name;
            m_Sound.m_Sourc.volume = m_Sound.m_Volume;
            m_Sound.m_Sourc.pitch = m_Sound.m_Pitch;
            m_Sound.m_Sourc.clip = m_Sound.m_Clip;
            m_Sound.m_Sourc.loop = m_Sound.m_Loop;
        }
    }


    public void f_PlaySound(string p_Name)
    {
        Sound m_play = Array.Find(m_Sounds, Sound => Sound.m_name == p_Name);
        m_play.m_Sourc.Play();
    }

    public void f_StopSound(string p_Name)
    {
        Sound m_play = Array.Find(m_Sounds, Sound => Sound.m_name == p_Name);
        m_play.m_Sourc.Stop();
    }
}
