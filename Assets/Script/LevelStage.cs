using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class LevelStage : MonoBehaviour
{
    public int m_TabLavel;
    public Sprite[] m_TabBG;
    public Sprite[] m_StarImage;
    public Image[] m_Stars;
    public TextMeshProUGUI m_TextLevel;
    public GameObject m_Locked;
    public Image m_CurrBG;
    public int m_StarSize;
    public Button m_Btn;




    void Update()
    {
        m_TextLevel.text = m_TabLavel.ToString();
        if (m_TabLavel <= LevelStageManager.instance.m_UnlockStage)
        {
            m_CurrBG.sprite = m_TabBG[0];
            m_Locked.SetActive(true);
            m_Btn.interactable = true;
        }
        else
        {
            m_CurrBG.sprite = m_TabBG[1];
            m_Locked.SetActive(false);
            m_Btn.interactable = false;
        }
        if (LevelStageManager.instance.m_UnlockStage >= m_TabLavel)
        {
            m_StarSize = LevelStageManager.instance.m_AllStart[m_TabLavel - 1];
        }

        GameManager.instance.f_StarDisplay(m_Stars, m_StarImage, m_StarSize);
    }

    public void f_Play()
    {

        LevelStageManager.instance.m_CurrLevel = m_TabLavel;
        FindObjectOfType<SoundManager>().f_PlaySound("click");
        SceneManager.LoadScene("GameScene");

    }



}
