using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Home : MonoBehaviour
{
    public GameObject m_LevelStage;
    public GameObject m_Home;
    public GameObject m_SkinTab;


    public bool home = true;

    private void Update()
    {
        // if (!home)ssss
        // {
        //     LevelStageManager.instance.f_StarInTab();s
        // }
    }
    public void f_LevelStage()
    {
        m_LevelStage.SetActive(!m_LevelStage.activeSelf);
        if (m_LevelStage.activeSelf)
        {
            m_Home.SetActive(false);
            home = false;

        }
        else
        {
            m_Home.SetActive(true);
            home = true;
        }
        FindObjectOfType<SoundManager>().f_PlaySound("click");
    }


    public void f_SkinsTab()
    {
        m_SkinTab.SetActive(!m_SkinTab.activeSelf);
    }

}
