using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class SaveData
{
    public int m_Coin;
    public List<int> m_WeaponOwned;
    public int m_9mm;
    public int m_55mm;
    public int m_12gauge;
    public int m_CurrUnlockStage;
    public List<int> m_StarStage = new List<int>();
    public List<int> m_MyWeaponsAmmo = new List<int>();

    public int m_FirstLogin;


}

